import os
from sys import argv
import requests
from time import sleep

params = {'n': 500, 'k': 10, 'sqpe': 100}
if len(argv) >= 2:
    params['n'] = int(argv[1])
if len(argv) >= 3:
    params['k'] = int(argv[2])
if len(argv) >= 4:
    params['sqpe'] = int(argv[3])

if params['n'] < 16 or params['n'] > 65536:
    print('Incorrect value! 16 <= n <= 65536')
    exit()
if params['k'] < 4 or params['k'] > 10:
    print('Incorrect value! 4 <= k <= 10')
    exit()
if params['sqpe'] > 0.25 * params['n']:
    print('Incorrect value! sqpe <= 0.25 * n')
    exit()

def get_data(params):
    # os.system(f"wget --timeout=2 https://www.cs.put.poznan.pl/pwawrzyniak/bio/bio.php?n={params['n']}&k={params['k']}&mode=basic&intensity=1&position=0&sqpe={params['sqpe']}&sqne=0&pose=0 > /dev/null &")
    page = requests.get(f"https://www.cs.put.poznan.pl/pwawrzyniak/bio/bio.php?n={params['n']}&k={params['k']}&mode=basic&intensity=1&position=0&sqpe={params['sqpe']}&sqne=0&pose=0")

    with open('bio.xml', 'wb') as f:
        f.write(page.content)
        f.close()

    data = 0
    for file in os.listdir():
        if file == 'bio.xml':
            data = 1
            os.system('make')
    if not data:
        print('No data!!!!')

def plot():
    pass

if __name__ == "__main__":
    for i in [600 for x in range(10)]:
        params['n'] = i
        get_data(params)