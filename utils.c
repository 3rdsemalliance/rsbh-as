#include <stdlib.h>
#include <string.h>

typedef struct{
  int key;
  int chain_length;
  int probe_length;
  char* start;
  int word_number;
  char** words;
  int* intensities;
} data;

int is_valid_oligo(data data, char* oligo){
  if(strlen(oligo) == data.probe_length){
    for(int i = 0; i < data.probe_length; ++i){
      switch(oligo[i]){
        case 'A':
        case 'C':
        case 'G':
        case 'T':
          continue;
        default:
          return 0;
      }
    }
    return 1;
  }
  return 0;
}