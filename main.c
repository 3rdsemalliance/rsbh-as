#include <libxml2/libxml/parser.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "src/global.h"
#include "src/algorithms.h"
#include "src/utils.h"
#include <fcntl.h>
// #include "src/parser.h"

int main(int argc, char **argv) {
  xmlDoc *document;
  xmlNode *root, *first_child, *node;
  Data main_data;
  char *filename;

  initialise_lookup();

  // for(int i = 0; i < RANGES; ++i){
  //   printf("Intensity %d means a minimum of %d matches and a maximum of %d matches.\n", i, INTENSITIES_LOOKUP[i].min, INTENSITIES_LOOKUP[i].max);
  // }

  if (argc < 2) {
    fprintf(stderr, "Usage: %s filename.xml\n", argv[0]);
    return 1;
  }
  //main_data main_data = parse(argv[1]);
  filename = argv[1];


  main_data.word_number = 0;

  document = xmlReadFile(filename, NULL, 0);
  root = xmlDocGetRootElement(document);
  // fprintf(stdout, "Root is <%s> (%i)\n", root->name, root->type);
  first_child = root->children;
  xmlAttr *attribute = root->properties;

  xmlChar *value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.key = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.chain_length = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.probe_length = strlen((char*)value);
  main_data.start = (char*)malloc(sizeof(char) * main_data.probe_length + 1);
  strcpy(main_data.start, (char*)value);
  xmlFree(value);
  attribute = attribute->next;

  xmlFree(attribute);

  int max_size = main_data.chain_length * 1.25;
  // printf("Max size: %d\n", max_size);

  main_data.words = (char**)malloc(sizeof(char*) * max_size);
  main_data.intensities = (int*)malloc(sizeof(int) * max_size);

  clock_t begin = clock();

  for (node = first_child; node; node = node->next) {
    // fprintf(stdout, "\t Child is <%s> (%i)\n", node->name, node->type);
    if (node->type == 1) {
      xmlNode *cell = node->children;
      for (;cell; cell = cell->next) {
        char* tmp = (char*)xmlNodeGetContent(cell);
        xmlAttr* intensity = cell->properties;
        if(is_valid_oligo(main_data, tmp)){
          ++main_data.word_number;
          int last_index = main_data.word_number - 1;
          xmlChar* value = xmlNodeListGetString(cell->doc, intensity->children, 1);
          main_data.words[last_index] = (char*)malloc(sizeof(char) * (main_data.probe_length + 1));
          strcpy(main_data.words[last_index], tmp);
          main_data.intensities[last_index] = atoi((char*) value);
          // printf("%s, %lu\n, intensity - %d", tmp, strlen(tmp), main_data.intensities[last_index]);
          xmlFree(value);
        }
        free(tmp);
        // xmlFree(intensity);
      }
      xmlFree(cell);
    }
  }

  main_data.words = (char**)realloc(main_data.words, sizeof(char*) * main_data.word_number);
  main_data.intensities = (int*)realloc(main_data.intensities, sizeof(int) * main_data.word_number);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("Time for xml read: %lf\n", time_spent);
  // main_data = parse("bio.xml");
  // printf("Key: %d\n", main_data.key);
  // printf("Target length: %d\n", main_data.chain_length);
  // printf("Initial oligo: %s\n", main_data.start);
  // printf("Probe length: %lu\n", strlen(main_data.start));
  // printf("Actual probe length: %d\n", main_data.probe_length);
  // printf("Data number: %d\n", main_data.word_number);
  // printf("Time taken: %lfs\n", time_spent);
  // for(int i = 0; i < main_data.word_number; ++i){
  //   printf("%d - %s : %d\n", i, main_data.words[i], main_data.intensities[i]);
  // }
  // free(INTENSITIES_LOOKUP);
  // fprintf(stdout, "...\n");
  xmlFreeDoc(document);
  
  // char* first = "UCGAGUCGG";
  // char* second = "GUCGGUAGU";
  // printf("Score of two given oligo: %d\n", score(first, second));
  // printf("Before merge: %s + %s\n", first, second);
  // printf("After merge: %s | %s + %s\n", merge(first, second, score(first, second)), first, second);

  // data* main_data_ptr = &main_data;
  // int result = open("results.txt", O_CREAT | )
  printf("In main: %d | %s\n", main_data.key, main_data.start);
  printf("\n\n---N = %d---\n\n", main_data.chain_length);
  begin = clock();
  Result resultGreedy = getGreedy(main_data);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  // printf("Greedy output: %s\n", resultGreedy.chain);
  printf("---GREEDY---\n");
  printf("Time taken: %lf\n", time_spent);
  printf("Length: %d\n", (int)strlen(resultGreedy.chain));
  printf("Used oligo: %d\n", resultGreedy.indices_length);
  free_result(resultGreedy);

  printf("---OPTIMAL---\n");
  begin = clock();
  Result resultOptimal = getOptimal(main_data);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  // printf("Optimal output: %s\n", resultOptimal.chain);
  printf("Time taken: %lf\n", time_spent);
  printf("Length: %d\n", (int)strlen(resultOptimal.chain));
  printf("Used oligo: %d\n", resultOptimal.indices_length);
  // for (int i = 0; i < resultOptimal.indices_length; i++){
  //   printf("%d, %d, %s\n", i, resultOptimal.indices[i], main_data.words[resultOptimal.indices[i]]);
  // }
  // printf("%ld\n", strlen(resultOptimal.chain));

  free_result(resultOptimal);

  free_main_data(main_data);
  free(INTENSITIES_LOOKUP);

  return 0;
}