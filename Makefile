CC=gcc
CFLAGS=-Wall -Wextra -Wpedantic -g -I/usr/include/libxml2
LIBFLAGS=-lxml2
OPTFLAGS=-O3
MAINFILE=main.c
SRC_DIR=src
SRCS=$(wildcard $(SRC_DIR)/*.c) $(MAINFILE)
OBJS=$(SRCS:.c=.o)
EXECUTABLES=main.exe
PARAMETERS=bio.xml

$(EXECUTABLES): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXECUTABLES) $(LIBFLAGS)
	./$(EXECUTABLES) $(PARAMETERS)
	rm -f $(EXECUTABLES) $(OBJS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

run:
	$(CC) $(CFLAGS) -o $(EXECUTABLES) $(SRCS) $(OPTFLAGS) $(LIBFLAGS)
	./$(EXECUTABLES) $(PARAMETERS)

clean:
	rm -f $(EXECUTABLES) $(OBJS)

