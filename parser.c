#include <libxml/parser.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "utils.c"

data parse(char* filename){
  xmlDoc *document;
  xmlNode *root, *first_child, *node;
  data data;

  char **probes;

  data.word_number = 0;

  document = xmlReadFile(filename, NULL, 0);
  root = xmlDocGetRootElement(document);
  fprintf(stdout, "Root is <%s> (%i)\n", root->name, root->type);
  first_child = root->children;
  xmlAttr *attribute = root->properties;

  xmlChar *value = xmlNodeListGetString(root->doc, attribute->children, 1);
  data.key = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  data.chain_length = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  data.probe_length = strlen((char*)value);
  data.start = (char*)malloc(sizeof(char) * data.probe_length + 1);
  strcpy(data.start, (char*)value);
  xmlFree(value);
  attribute = attribute->next;

  xmlFree(attribute);

  // data.intensities = (int*)malloc(sizeof(int) * data.)

  data.words = (char**)malloc(sizeof(char*) * 0);
  data.intensities = (int*)malloc(sizeof(int) * 0);

  for (node = first_child; node; node = node->next) {
    // fprintf(stdout, "\t Child is <%s> (%i)\n", node->name, node->type);
    if (node->type == 1) {
      xmlNode *cell = node->children;
      for (;cell; cell = cell->next) {
        char* tmp = (char*)xmlNodeGetContent(cell);
        xmlAttr* intensity = cell->properties;
        if(is_valid_oligo(data, tmp)){
          ++data.word_number;
          int last_index = data.word_number - 1;
          xmlChar* value = xmlNodeListGetString(cell->doc, intensity->children, 1);
          data.words = (char**)realloc(data.words, sizeof(char*) * data.word_number);
          data.words[last_index] = (char*)malloc(sizeof(char) * (data.probe_length + 1));
          strcpy(data.words[last_index], tmp);
          data.intensities = (int*)realloc(data.intensities, sizeof(int) * data.word_number);
          data.intensities[last_index] = atoi((char*) value);
          // printf("%s, %lu\n, intensity - %d", tmp, strlen(tmp), data.intensities[last_index]);
          xmlFree(value);
        }
        free(tmp);
        // xmlFree(intensity);
      }
      xmlFree(cell);
    }
  }
  free(data.start);
//   fprintf(stdout, "...\n");
  xmlFreeDoc(document);
  return data;
}