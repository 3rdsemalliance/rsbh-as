# RSBH+AS

Repetition Sequencing by Hybridization - Automatic Solver

# How to use the programs?

To run the search algorithms on existing data, simply call `make`.

To generate a new input file, run `tester.py`. Optional arguments are (in this order): target DNA chain length, DNA probe length, number of positive errors.