#ifndef ALGORITHMS_H
#define ALGORITHMS_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"
#include "utils.h"

char* merge(char* dest, char* src, int n);
int score(char* oligo1, char* oligo2);
Result getGreedy(Data data);
Result getOptimal(Data data);

Successors_data generate_successors(Data data);
Result get_result_from_indexes(Data data, int* path);
Hamiltonian_path hamiltonian(Successors_data successors_data, Data data, Hamiltonian_path hamiltonian_path);
Hamiltonian_path create_new_hamiltonian_path(Data data, Hamiltonian_path hamiltonian_path);
int find_oligo_index(char* oligo, Data data);

#endif