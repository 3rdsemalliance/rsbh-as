#include "parser.h"

Data parse(char* filename){
  xmlDoc *document;
  xmlNode *root, *first_child, *node;
  Data main_data;
  
  main_data.word_number = 0;

  document = xmlReadFile(filename, NULL, 0);
  root = xmlDocGetRootElement(document);
  // fprintf(stdout, "Root is <%s> (%i)\n", root->name, root->type);
  first_child = root->children;
  xmlAttr *attribute = root->properties;

  xmlChar *value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.key = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.chain_length = atoi((char*) value);
  xmlFree(value);
  attribute = attribute->next;

  value = xmlNodeListGetString(root->doc, attribute->children, 1);
  main_data.probe_length = strlen((char*)value);
  main_data.start = (char*)malloc(sizeof(char) * main_data.probe_length + 1);
  strcpy(main_data.start, (char*)value);
  xmlFree(value);
  attribute = attribute->next;

  xmlFree(attribute);

  int max_size = main_data.chain_length * 1.25;
  printf("Max size: %d\n", max_size);

  main_data.words = (char**)malloc(sizeof(char*) * max_size);
  main_data.intensities = (int*)malloc(sizeof(int) * max_size);

  for (node = first_child; node; node = node->next) {
    // fprintf(stdout, "\t Child is <%s> (%i)\n", node->name, node->type);
    if (node->type == 1) {
      xmlNode *cell = node->children;
      for (;cell; cell = cell->next) {
        char* tmp = (char*)xmlNodeGetContent(cell);
        xmlAttr* intensity = cell->properties;
        if(is_valid_oligo(main_data, tmp)){
          ++main_data.word_number;
          int last_index = main_data.word_number - 1;
          xmlChar* value = xmlNodeListGetString(cell->doc, intensity->children, 1);
          main_data.words[last_index] = (char*)malloc(sizeof(char) * (main_data.probe_length + 1));
          strcpy(main_data.words[last_index], tmp);
          main_data.intensities[last_index] = atoi((char*) value);
          // printf("%s, %lu\n, intensity - %d", tmp, strlen(tmp), main_data.intensities[last_index]);
          xmlFree(value);
        }
        free(tmp);
        // xmlFree(intensity);
      }
      xmlFree(cell);
    }
  }

  main_data.words = (char**)realloc(main_data.words, sizeof(char*) * main_data.word_number);
  main_data.intensities = (int*)realloc(main_data.intensities, sizeof(int) * main_data.word_number);
//   fprintf(stdout, "...\n");
  xmlFreeDoc(document);
  return main_data;
}