#ifndef PARSER_H
#define PARSER_H
#include <libxml2/libxml/parser.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "global.h"
#include "utils.h"

Data parse(char* filename);

#endif