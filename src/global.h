#ifndef GLOBAL_H
#define GLOBAL_H
#define RANGES 10

typedef struct{
  int min;
  int max;
} Range;

typedef struct{
  int key;
  int chain_length;
  int probe_length;
  char* start;
  int word_number;
  char** words;
  int* intensities;
} Data;

typedef struct{
  char* chain;
  int* indices;
  int indices_length;
} Result;

typedef struct{
  int** successors;
  int* successors_length;
} Successors_data;

typedef struct{
  int* path;
  int path_len;
  int vertex_idx;
  int* number_of_visits;
  int backtrack;
} Hamiltonian_path;

extern Range* INTENSITIES_LOOKUP;

#endif