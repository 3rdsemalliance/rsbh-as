#include "utils.h"

void initialise_lookup(){
  INTENSITIES_LOOKUP = (Range*)malloc(sizeof(Range) * RANGES);
  INTENSITIES_LOOKUP[0] = (Range){0, 0};
  INTENSITIES_LOOKUP[1] = (Range){1, 1};
  INTENSITIES_LOOKUP[2] = (Range){1, 1};
  INTENSITIES_LOOKUP[3] = (Range){1, 2};
  INTENSITIES_LOOKUP[4] = (Range){2, 2};
  INTENSITIES_LOOKUP[5] = (Range){2, 3};
  INTENSITIES_LOOKUP[6] = (Range){3, 4};
  INTENSITIES_LOOKUP[7] = (Range){3, 6};
  INTENSITIES_LOOKUP[8] = (Range){4, 8};
  INTENSITIES_LOOKUP[9] = (Range){6, 9}; //can technically be used infinitely many times but we want diverse solutions
}

int is_valid_oligo(Data data, char* oligo){
  if(strlen(oligo) == (size_t)data.probe_length){
    for(int i = 0; i < data.probe_length; ++i){
      switch(oligo[i]){
        case 'A':
        case 'C':
        case 'G':
        case 'T':
          continue;
        default:
          return 0;
      }
    }
    return 1;
  }
  return 0;
}

void free_result(Result result){
  free(result.chain);
  free(result.indices);
}

void free_main_data(Data data){
  for(int i = 0; i < data.word_number; ++i){
    free(data.words[i]);
  }
  free(data.words);
  free(data.intensities);
  free(data.start);
}

void free_successors_data(Successors_data successors_data, int word_number){
    for (int i = 0; i < word_number; i++){
        free(successors_data.successors[i]);
    }
    free(successors_data.successors);
    free(successors_data.successors_length);
}

void free_hamiltonian_path(Hamiltonian_path hamiltonian_path){
    free(hamiltonian_path.path);
    free(hamiltonian_path.number_of_visits);
}