#include "algorithms.h"
#include "global.h"

char* merge(char* dest, char* src, int n){
    int desired_length = strlen(src) - n;
    char* result = (char*)malloc(sizeof(char) * (strlen(dest) + desired_length + 1));
    strcpy(result, dest);
    strncat(result, src + n, desired_length);
    free(dest);
    // printf("%s\n", result);
    return result;
}

int score(char* oligo1, char* oligo2){
    if(strlen(oligo1) != strlen(oligo2)){
        // printf("Nucleotides must be of equal length!\n");
        return 0;
    }
    int max_score = strlen(oligo1) - 1;
    int flag;
    for(int i = max_score; i > 0; --i){
        flag = 1;
        for(int j = 0; j < i; ++j){
            int formula = max_score - i + j + 1;
            // printf("Comparing: %c and %c\n", oligo1[formula], oligo2[j]);
            if(oligo1[formula] != oligo2[j]){
                flag = 0;
                break;
            }
        }
        if(flag){
            // printf("%d is the score\n", i);
            return i;
        }
        // printf("%d was not the score\n", i);
    }
    // printf("The score is 0\n");
    return 0;
}

Result getGreedy(Data data){
    // printf("In function: %d | %s\n", data.key, data.start);
    Result result;
    result.chain = (char*)malloc(sizeof(char) * data.chain_length);
    result.indices = (int*)malloc(sizeof(int) * (data.chain_length - data.probe_length + 1));
    result.indices_length = data.chain_length - data.probe_length + 1;
    int index = -1;
    for(int i = 0; i < result.indices_length; ++i){
        result.indices[i] = -1;
    }
    for(int i = 0; i < data.word_number; ++i){
        if(strcmp(data.words[i], data.start) == 0){
            // printf("%s, %s\n", data.start, data.words[i]);
            result.indices[0] = i;
            strcpy(result.chain, data.words[i]);
            index = i;
            // printf("%d\n", i);
            break;
        }
    }
    // if (index != -1){
    //     printf("%s - %s|%d\n", data.start, data.words[index], index);
    // }
    int** score_matrix = (int**)malloc(sizeof(int*) * data.word_number);
    for(int i = 0; i < data.word_number; ++i){
        score_matrix[i] = (int*)malloc(sizeof(int) * data.word_number);
    }

    for(int i = 0; i < data.word_number; ++i){
        for(int j = 0; j < data.word_number; ++j){
            score_matrix[i][j] = score(data.words[i], data.words[j]);
        }
    }

    int* word_use_count = (int*)malloc(sizeof(int) * data.word_number);
    for(int i = 0; i < data.word_number; ++i){
        word_use_count[i] = 0;
    }
    word_use_count[index] = 1;

    int max_score, max_index, no_continuation = 0;
    int current_result_index = 1;
    while((int)strlen(result.chain) < data.chain_length){
        no_continuation = 1;
        max_score = -1;
        for(int i = 0; i < data.word_number; ++i){
            if(score_matrix[index][i] > max_score && index != i && word_use_count[i] < INTENSITIES_LOOKUP[data.intensities[i]].max && (int)strlen(result.chain) + data.probe_length - score_matrix[index][i] <= data.chain_length){
                max_score = score_matrix[index][i];
                max_index = i;
                no_continuation = 0;
            }
        }
        if(score_matrix[index][index] > max_score && word_use_count[index] < INTENSITIES_LOOKUP[data.intensities[index]].max && (int)strlen(result.chain) + data.probe_length - score_matrix[index][index] <= data.chain_length){
            max_score = score_matrix[index][index];
            max_index = index;
            no_continuation = 0;
        }
        if(no_continuation){
            break;
        }
        result.chain = merge(result.chain, data.words[max_index], score_matrix[index][max_index]);
        ++word_use_count[max_index];
        result.indices[current_result_index++] = max_index;
        index = max_index;
    }
    int correct = 0, incorrect = 0;
    for(int i = 0; i < data.word_number; ++i){
        // if(word_use_count[i] > 0){
        //     printf("%s: used %d times, range: [%d, %d]\n", data.words[i], word_use_count[i], INTENSITIES_LOOKUP[data.intensities[i]].min, INTENSITIES_LOOKUP[data.intensities[i]].max);
        // }
        if(word_use_count[i] >= INTENSITIES_LOOKUP[data.intensities[i]].min && word_use_count[i] <= INTENSITIES_LOOKUP[data.intensities[i]].max){
            ++correct;
            continue;
        }
        ++incorrect;
    }
    double percentage_error = (double)incorrect / (double)(incorrect + correct);
    // printf("Number of nucleotides used correctly: %d\n", correct);
    // printf("Number of nucleotides used incorrectly: %d\n", incorrect);
    // printf("Percentage error: %lf\n", percentage_error);
    printf("GREEDY: Percentage error is %d / %d = %lf%%\n", incorrect, incorrect + correct, percentage_error * 100);
    if(percentage_error <= 0.25){
        printf("Since the positive error count can be at most equal to 1/4 of the oligonucleotides' number, the solution is feasible.\n");
    }
    else{
        printf("The solution contains more errors than permissible (there can not be more than 1/4 of erroneous oligonucleotides), so it is not feasible.\n");
    }

    for(int i = 0; i < data.word_number; ++i){
        free(score_matrix[i]);
    }
    free(score_matrix);
    free(word_use_count);
    return result;
}

Result getOptimal(Data data){
    Result result;
    Successors_data succesors_data = generate_successors(data);
    Hamiltonian_path hamiltonian_path = {
        .path = (int*)malloc(sizeof(int) * 0),
        .path_len = 0,
        .vertex_idx = find_oligo_index(data.start, data),
        .number_of_visits = (int*)calloc(data.word_number, sizeof(int)),
        .backtrack = 0
    };
    if (hamiltonian_path.vertex_idx != -1){
        Hamiltonian_path new_hamiltonian_path = hamiltonian(succesors_data, data, hamiltonian_path);
        if (new_hamiltonian_path.backtrack == 0){
            result = get_result_from_indexes(data, new_hamiltonian_path.path);
        }
        else {
            char* chain = (char*)malloc(sizeof(char) * 23);
            strcpy(chain, "ERROR - path not found");
            result.chain = chain;
            result.indices = NULL;
            result.indices_length = 0;
        }
    }
    else {
        char* chain = (char*)malloc(sizeof(char) * 38);
        strcpy(chain, "ERROR - start oligo not found in data");
        result.chain = chain;
        result.indices = NULL;
        result.indices_length = 0;
    }
    free_hamiltonian_path(hamiltonian_path);
    free_successors_data(succesors_data, data.word_number);
    return result;
}

Result get_result_from_indexes(Data data, int* path){
    Result result;
    result.indices_length = data.chain_length - data.probe_length + 1;
    char* chain = (char*)malloc(sizeof(char) * (data.chain_length + 1));
    memset(chain, 0, sizeof(char) * (data.chain_length + 1));
    for (int i = 0; i < result.indices_length; i++){
        char* oligo = data.words[path[i]];
        if (i == 0){
            strncpy(chain, oligo, data.probe_length);
        }
        else{
            strncat(chain, &oligo[strlen(oligo)-1], 1);
        }
    }
    result.chain = chain;
    result.indices = path;
    return result;
}

Hamiltonian_path create_new_hamiltonian_path(Data data, Hamiltonian_path hamiltonian_path){
    int* new_path = (int*)malloc(sizeof(int) * (hamiltonian_path.path_len + 1));
    memcpy(new_path, hamiltonian_path.path, sizeof(int) * (hamiltonian_path.path_len));
    new_path[hamiltonian_path.path_len] = hamiltonian_path.vertex_idx;

    int new_path_len = hamiltonian_path.path_len + 1;
    
    int* new_number_of_visits = (int*)malloc(sizeof(int) * (data.word_number + 1));
    memcpy(new_number_of_visits, hamiltonian_path.number_of_visits, sizeof(int) * data.word_number);
    new_number_of_visits[hamiltonian_path.vertex_idx]++;

    Hamiltonian_path new_hamiltonian_path = {
        .path = new_path,
        .path_len = new_path_len,
        .vertex_idx = hamiltonian_path.vertex_idx,
        .number_of_visits = new_number_of_visits,
        .backtrack = hamiltonian_path.backtrack
    };
    return new_hamiltonian_path;
}

Hamiltonian_path hamiltonian(Successors_data successors_data, Data data, Hamiltonian_path hamiltonian_path){
    Hamiltonian_path new_hamiltonian_path = create_new_hamiltonian_path(data, hamiltonian_path);

    if (new_hamiltonian_path.number_of_visits[new_hamiltonian_path.vertex_idx] > INTENSITIES_LOOKUP[data.intensities[new_hamiltonian_path.vertex_idx]].max){
        // printf("Too many visits, backtrack\n");
        free_hamiltonian_path(new_hamiltonian_path);
        new_hamiltonian_path.backtrack = 1;
        return new_hamiltonian_path;
    }

    if (new_hamiltonian_path.path_len == data.chain_length - data.probe_length + 1) {
        // + possible check how many oligos are not in its range -> count number of possible positive errors
        int error_num = 0;
        for (int i = 0; i < data.word_number; i++){
            if (new_hamiltonian_path.number_of_visits[i] < INTENSITIES_LOOKUP[data.intensities[i]].min){
                error_num++;
            }
        }
        // printf("Numbers of errors: %d\n", error_num);
        double percentage_error = (double)error_num / (double)data.word_number;
        if (percentage_error > 0.25){
            // printf("Percentage of error is to high! Backtracking! (%lf%%)\n", percentage_error * 100);
            free_hamiltonian_path(new_hamiltonian_path);
            new_hamiltonian_path.backtrack = 1;
            return new_hamiltonian_path;
        }
        else {
            free(new_hamiltonian_path.number_of_visits);
            return new_hamiltonian_path;
        }
    }

    for (int i = 0; i < successors_data.successors_length[new_hamiltonian_path.vertex_idx] + 1; i++){
        int nextVertex = successors_data.successors[new_hamiltonian_path.vertex_idx][i];
        // printf("%d, Next try for %d, %d, max tries: %d\n", i, new_hamiltonian_path.vertex_idx, nextVertex, successors_data.successors_length[new_hamiltonian_path.vertex_idx]);
        if (nextVertex == -1){
            // printf("Dead end\n");
            free_hamiltonian_path(new_hamiltonian_path);
            new_hamiltonian_path.backtrack = 1;
            return new_hamiltonian_path;
        }
        else{
            new_hamiltonian_path.vertex_idx = nextVertex;
            Hamiltonian_path possibleNextHamiltonian = hamiltonian(successors_data, data, new_hamiltonian_path);
            if (possibleNextHamiltonian.backtrack == 0){
                // printf("Getting deeper\n");
                free_hamiltonian_path(new_hamiltonian_path);
                return possibleNextHamiltonian;
            }
            // printf("Try to get next successor\n");
            new_hamiltonian_path.vertex_idx = hamiltonian_path.vertex_idx;
        }
    }
    // printf("No more successors\n");
    free_hamiltonian_path(new_hamiltonian_path);
    new_hamiltonian_path.backtrack = 1;
    return new_hamiltonian_path;
}

int find_oligo_index(char* oligo, Data data){
    for (int i = 0; i < data.word_number; i++){
        if (strcmp(data.words[i], oligo) == 0) return i;
    }
    return -1;
}

Successors_data generate_successors(Data data){
  Successors_data succesors_data;
  int** successors = (int**)malloc(sizeof(int*) * data.word_number);
  for (int i = 0; i < data.word_number; i++){
    successors[i] = (int*)malloc(sizeof(int) * 0);
  }
  int* successors_number = (int*)calloc(data.word_number, sizeof(int));

  for (int i = 0; i < data.word_number; i++){
    int current_successors_number = 0;
    for (int j = 0; j < data.word_number; j++){
      if (score(data.words[i], data.words[j]) == data.probe_length - 1 && i != j){
        successors[i] = realloc(successors[i], sizeof(int) * ++current_successors_number);
        successors[i][current_successors_number-1] = j;
      }
    }
    // if intensity of i can be merge with itself (max count number > 1)
    // score still has to be maximum, eg. : AAAA can be merged with AAAA if intensity of signal is strong enough
    if (INTENSITIES_LOOKUP[data.intensities[i]].max > 1 && score(data.words[i], data.words[i]) == data.probe_length - 1){
      successors[i] = realloc(successors[i], sizeof(int) * ++current_successors_number);
      successors[i][current_successors_number-1] = i;
    }
    successors[i] = realloc(successors[i], sizeof(int) * (current_successors_number + 1));
    successors[i][current_successors_number] = -1;
    successors_number[i] = current_successors_number;
    // printf("%d, succ number %d\n", i, successors_number[i]);
  }
  succesors_data.successors = successors;
  succesors_data.successors_length = successors_number;
  return succesors_data;
}