#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <string.h>
#include "global.h"

void initialise_lookup();
int is_valid_oligo(Data data, char* oligo);
void free_result(Result result);
void free_main_data(Data data);
void free_hamiltonian_path(Hamiltonian_path hamiltonian_path);
void free_successors_data(Successors_data successors_data, int word_number);

#endif